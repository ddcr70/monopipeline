# https://registry.terraform.io/providers/hashicorp/aws/latest/docs#argument-reference
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = var.env_region
}