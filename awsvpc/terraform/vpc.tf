resource "aws_vpc" "vpc_0" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  instance_tenancy     = "default"
  tags = {
    env_name_group = var.env_name
    function = "vpc"
  }
}

resource "aws_subnet" "subnet_0" {
  vpc_id                  = aws_vpc.vpc_0.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  tags = {
    env_name_group = var.env_name
    function = "generalsubnet"
  }
}